<%@page import="java.util.List"%>
<%@page import="br.com.senac.modelo.Cidade"%>
<%@page import="br.com.senac.banco.CidadeDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />




<%
    CidadeDAO dao = new CidadeDAO();
    List<Cidade> lista = dao.listarTodos();
%>

<div class="container">
    <fieldset>
        <legend>Lista de Cidades</legend>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Cidade</th>
                </tr>
            </thead>
            <tbody>


                <% for (Cidade c : lista) { %>

                <tr>
                    <td><% out.print(c.getCodigo()); %></td>
                    <td><% out.print(c.getNome()); %></td>
                </tr>

                <% }%>

            </tbody>
        </table>

    </fieldset>

</div>

<jsp:include page="../footer.jsp" />