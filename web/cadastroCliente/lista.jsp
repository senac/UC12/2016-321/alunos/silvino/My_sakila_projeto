


<%@page import="br.com.senac.modelo.Cliente"%>
<%@page import="java.util.List"%>
<%@page import="br.com.senac.banco.ClienteDAO"%>
<jsp:include page="../header.jsp" />

<%
    ClienteDAO dao = new ClienteDAO();
    List<Cliente> lista = dao.listarTodos();

%>


<div class="container">
    <fieldset>
        <legend>Lista de Clientes</legend> 

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>C�digo</th>
                    <th>Primeiro Nome</th>
                    <th>Ultimo Nome</th>
                    <th>Endereco</th>
                    <th>Telefone</th>
                </tr>
            </thead>
            <tbody>
                <% for (Cliente c : lista) {%>
                <tr>
                    <td><%= c.getCodigo() %></td>
                    <td><%= c.getPrimeiroNome()%></td>
                    <td><%= c.getUltimoNome()%> </td>
                    <td><%= c.getEndereco() %> </td>
                    <td><%= c.getEndereco().getTelefone() %> </td>
                </tr>

                <%}%>
            </tbody>
        </table>
    </fieldset>
</div>


<jsp:include page="../footer.jsp" />