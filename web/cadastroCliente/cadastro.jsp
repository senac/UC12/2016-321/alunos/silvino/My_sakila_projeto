
<%@page import="br.com.senac.modelo.Cliente"%>
<%@page import="br.com.senac.modelo.Cidade"%>
<%@page import="java.util.List"%>
<%@page import="br.com.senac.banco.CidadeDAO"%>
<jsp:include page="../header.jsp" />

<%
    CidadeDAO cidadeDAO = new CidadeDAO();

    List<Cidade> listaCidade = cidadeDAO.listarTodos();

%>

<script type="text/javascript">

    function buscaPais() {

        /*
         var dropdownCidade = document.getElementById('cidade') ; 
         var codigo = dropdownCidade.value;
         alert(codigo);
         */

        //utilizando Jquery
        var codigo = $('#cidade').val();

        if (codigo !== 0) {
            //vai buscar ....

            $.get(
                    "/Sakila/cadastroCidade/cidade.do",
                    {
                        'codigo': codigo
                    },
                    function (data) {
                        $('#pais').val(data);

                    });

        } else {
            //limpa o campo pais
            $('#pais').val('');

        }


    }

</script>







<div class="container">
    <fieldset>
        <legend>Cadastro de Clientes</legend>
        <form class="form-horizontal" action="./cliente.do" method="post">

            <input type="hidden" name="codigo" value="<%= request.getAttribute("cliente") == null ? "0" : request.getParameter("codigo")%>" />

            <form action="#" class="form-horizontal">
                        <div class="panel panel-default">
                            <legend>Dados Pessoais</legend>


                            <form class="form-horizontal" >
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="codigo">C�digo:</label>
                                    <div class="col-sm-2">
                                        <input readonly="true" type="text" class="form-control" id="codigo"  value="<%= request.getAttribute("cliente") == null ? "" : ((Cliente) request.getAttribute("cliente")).getCodigo()%>"   >

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="primeiroNome">Primeiro nome:</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="primeiroNome" placeholder="Entre com o primeiro nome" name="primeiroNome">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="ultimoNome">Ultimo Nome:</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="ultomoNome" placeholder="Entre com o Ultimo Nome" name="ultimoNome">
                                    </div>
                                </div>





                                <legend>Endere�o</legend>

                                <div class="form-group">
                                    <label for="cep" class="col-sm-2 control-label">CEP:</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="cep" placeholder="Entre com o CEP" name="cep">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="endereco" class="col-sm-2 control-label">Endere�o</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="endereco" placeholder="Entre com o Endere�o" name="endreco">
                                    </div>
                                </div>                        


                                <div class="form-group">
                                    <label for="bairro" class="col-sm-2 control-label">Bairro:</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="bairro" placeholder="Entre com o Bairro " name="bairro">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="complemento">Complemento:</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control" id="complemento" placeholder="Entre com o complemento" name="complemento">
                                    </div>


                                    <label class="control-label col-sm-1" for="cidade">Cidade:</label>
                                    <div class="col-sm-2">
                                        <select class="form-control " id="cidade" name="cidade">
                                            <option value="0">Selecione...</option>

                                            <% for (Cidade c : listaCidade) {%>

                                            <option value="<%= c.getCodigo()%>" ><%= c.getNome()%></option>

                                            <% }%>

                                        </select>
                                    </div>
                                    <label class="control-label col-sm-1" for="pais">Pa�s:</label>
                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" id="pais" name="pais" readonly="true">
                                    </div>
                                </div>








                                <legend>Contato</legend>
                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">Email:</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="email" placeholder="Entre com o Email" name="email">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="telefone" class="col-sm-2 control-label">Telefone:</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="telefone" placeholder="Entre com o Telefone" name="telefone">
                                    </div>
                                </div>




                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12"> 
                                                <input type="submit"   class="btn btn-primary col-" value="Salvar" />
                                                <input type="reset"    class="btn btn-danger" value="Cancelar" />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                        </div>
                        </div>
                    </form> 



                    <%
                        if (request.getAttribute("mensagem") != null) {
                            out.print(request.getAttribute("mensagem"));
                        } else {
                            out.print("");
                        }
                    %>


                    <jsp:include page="../footer.jsp" />